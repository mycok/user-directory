[![Build Status](https://travis-ci.com/mycok/user-directory.svg?branch=develop)](https://travis-ci.com/mycok/user-directory)
[![Test Coverage](https://api.codeclimate.com/v1/badges/864e01c3ac42d384d2b9/test_coverage)](https://codeclimate.com/github/mycok/user-directory/test_coverage)
[![Maintainability](https://api.codeclimate.com/v1/badges/864e01c3ac42d384d2b9/maintainability)](https://codeclimate.com/github/mycok/user-directory/maintainability)
#### User Directory Application
